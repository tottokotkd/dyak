defmodule Dyak.PageController do
  use Dyak.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
