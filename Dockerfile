# This file is a template, and might need editing before it works on your project.
FROM elixir:1.4.5

WORKDIR /phoenix

# args
ENV MIX_ENV docker

# phoenix
RUN mix local.hex --force \
    && mix local.rebar --force \
    && mix archive.install https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez --force

# dyak
COPY mix.exs /phoenix/
RUN mix deps.get --force --only prod
COPY . /phoenix
RUN mix compile && mix phoenix.digest

CMD ["mix", "phoenix.server"]

EXPOSE 4000
