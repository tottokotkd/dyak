use Mix.Config

config :dyak, Dyak.Endpoint,
  http: [port: 4001],
  server: false

config :logger, level: :warn

config :dyak, Dyak.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "gitlab",
  password: "gitlab",
  database: "dyak_gitlab",
  hostname: "postgres",
  pool_size: 10
