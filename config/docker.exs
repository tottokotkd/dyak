use Mix.Config

config :dyak, Dyak.Endpoint,
  http: [port: 4000],
  url: [host: "example.com", port: 80],
  cache_static_manifest: "priv/static/manifest.json"

config :logger, level: :info

config :dyak, Dyak.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "dyak_dev",
  hostname: "postgres",
  pool_size: 10
